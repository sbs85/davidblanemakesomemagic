<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="utf-8">
    <title>Add Phone</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://getbootstrap.com/dist/css/bootstrap-responsive.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h4><a href="/logout">Logout</a></h4>
    <h4><a href="/contacts/${contact.id}.html">Back</a></h4>
    <h1>Add phone</h1>

    <table class="table table-bordered table-striped">
        <tr><td><form:form action="/phones/add.html?contact_id=${contact.id}" method="post" cssClass="form-horizontal registrationForm">

            <div class="form-group">
                <label class="col-sm-2 control-label">${contact.name}</label>
            </div>

            <div class="form-group">
                <label for="phonenumber" class="col-sm-2 control-label">Phone:</label>
                <div class="col-sm-10">
                    <input type="text" id="phonenumber" name="phonenumber" class="form-control" />
                </div>
            </div>

            <div class="form-group">
                <label for="phonetype_id" class="col-sm-2 control-label">Phone type:</label>
                <div class="col-sm-10">
                    <select id="phonetype_id" name="phonetype_id" class="form-control">
                        <option value="0" selected>(none)<option>
                        <c:forEach var="phonetype" items="${phonetypes}">
                        <option value="${phonetype.id}">${phonetype.phoneType}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-2">
                    <input type="submit" value="Save" class="btn btn-lg btn-primary" />
                </div>
            </div>
        </form:form></td></tr>

        </table>
</div>
</body>
</html>
