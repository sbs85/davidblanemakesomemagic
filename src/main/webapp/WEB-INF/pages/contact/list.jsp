<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>--%>

<html>
<head>
    <meta charset="utf-8">
    <title>Phone Book</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://getbootstrap.com/dist/css/bootstrap-responsive.min.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <h4><a href="/logout">Logout</a></h4>
    <c:choose>
        <%--<c:when test="${not empty foundContacts || not empty foundPhones}">--%>
        <c:when test="${not empty foundPhones}">
            <h4><a href="/contacts.html">Back to contacts</a></h4>
            <h3>Search results</h3>
        </c:when>
        <c:when test="${empty param.searchCriteria}">
            <h3>Contacts</h3>
        </c:when>
        <c:otherwise>
            <h4><a href="/contacts.html">Back to contacts</a></h4>
            <h3>No results for search</h3>
        </c:otherwise>
    </c:choose>
    <form class="navbar-form navbar-left" action="/contacts/find.html" role="search">
        <div class="form-group">
            <input id="searchCriteria" name="searchCriteria" type="text" class="form-control" placeholder="Search by name or phone" value="${param.searchCriteria}">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>


    <c:choose>
        <%--search results--%>
        <c:when test="${not empty foundPhones}">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Phone type</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${foundContacts}" var="fcontact">
                        <c:forEach items="${fcontact.phones}" var="fcphone">
                            <tr>
                                <td><a href="/contacts/edit/${fcontact.id}.html">${fcontact.name}</a></td>
                                <td><a href="/phones/edit/${fcphone.id}.html">${fcphone.number}</a></td>
                                <td><a href="/phones/edit/${fcphone.id}.html">${fcphone.phoneType.phoneType}</a></td>
                            </tr>
                        </c:forEach>
                    </c:forEach>
                    <c:forEach items="${foundPhones}" var="fphone">
                        <tr>
                            <td><a href="/contacts/edit/${fphone.contact.id}.html">${fphone.contact.name}</a></td>
                            <td><a href="/phones/edit/${fphone.id}.html">${fphone.number}</a></td>
                            <td><a href="/phones/edit/${fphone.id}.html">${fphone.phoneType.phoneType}</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:when>
        <%--end search results--%>

        <%--contacts list--%>
        <c:when test="${empty param.searchCriteria}">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                            <th> <a href="/contacts/add.html">Add new contact</a> </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${contacts}" var="contact">
                    <tr>
                        <td>
                            <a href="<spring:url value="/contacts/${contact.id}.html"/> ">
                                    ${contact.name}
                            </a>
                        </td>
                                      <td>
                                        <form:form action="/contacts/deletecontact/${contact.id}.html" method="post"><input type="submit"
                                                                                             class="btn btn-danger btn-mini"
                                                                                             value="Delete"/>
                                        </form:form>
                                      </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <%--end contacts list--%>
    </c:choose>


    <c:choose>
        <c:when test="${not empty foundPhones}">
            <%--<c:url var="searchCriteriaUrl" value="?searchCriteria=${fn:escapeXml(param.searchCriteria)}"/>--%>
            <c:url var="findUrl" value="/find"/>
            <c:url var="firstUrl" value="/contacts${findUrl}/page/1.html">
                <c:param name="searchCriteria" value="${param.searchCriteria}"/>
            </c:url>
            <c:url var="lastUrl" value="/contacts${findUrl}/page/${totalPages}.html">
                <c:param name="searchCriteria" value="${param.searchCriteria}"/>
            </c:url>
            <c:url var="prevUrl" value="/contacts${findUrl}/page/${currentIndex - 1}.html">
                <c:param name="searchCriteria" value="${param.searchCriteria}"/>
            </c:url>
            <c:url var="nextUrl" value="/contacts${findUrl}/page/${currentIndex + 1}.html">
                <c:param name="searchCriteria" value="${param.searchCriteria}"/>
            </c:url>
        </c:when>
        <c:when test="${empty param.searchCriteria}">
            <c:url var="findUrl" value=""/>
            <c:url var="firstUrl" value="/contacts${findUrl}/page/1.html"/>
            <c:url var="lastUrl" value="/contacts${findUrl}/page/${totalPages}.html"/>
            <c:url var="prevUrl" value="/contacts${findUrl}/page/${currentIndex - 1}.html"/>
            <c:url var="nextUrl" value="/contacts${findUrl}/page/${currentIndex + 1}.html"/>
        </c:when>
    </c:choose>

    <c:if test="${not empty foundPhones || not empty contacts}">
        <nav>
            <ul class="pagination">
                <c:choose>
                    <c:when test="${currentIndex == 1}">
                        <li class="disabled"><a href="#">&lt;&lt;</a></li>
                        <li class="disabled"><a href="#">&lt;</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${firstUrl}">&lt;&lt;</a></li>
                        <li><a href="${prevUrl}">&lt;</a></li>
                    </c:otherwise>
                </c:choose>
                <c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
                    <c:choose>
                        <c:when test="${not empty foundPhones}">
                            <c:url var="pageUrl" value="/contacts${findUrl}/page/${i}.html">
                                <c:param name="searchCriteria" value="${param.searchCriteria}"/>
                            </c:url>
                        </c:when>
                        <c:when test="${empty param.searchCriteria}">
                            <c:url var="pageUrl" value="/contacts${findUrl}/page/${i}.html"/>
                        </c:when>
                    </c:choose>
                    <c:choose>
                        <c:when test="${i == currentIndex}">
                            <li class="active"><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <c:choose>
                    <c:when test="${currentIndex == totalPages}">
                        <li class="disabled"><a href="#">&gt;</a></li>
                        <li class="disabled"><a href="#">&gt;&gt;</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${nextUrl}">&gt;</a></li>
                        <li><a href="${lastUrl}">&gt;&gt;</a></li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </nav>
    </c:if>
</div>
</body>
</html>