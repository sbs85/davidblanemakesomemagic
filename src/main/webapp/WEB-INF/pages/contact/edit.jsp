<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
  <meta charset="utf-8">
  <title>Edit Contact</title>

  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="http://getbootstrap.com/dist/css/bootstrap-responsive.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
  <h4><a href="/logout">Logout</a></h4>
  <h4><a href="/contacts/${contact.id}.html">Contact details</a></h4>
  <h1>Edit contact</h1>

  <table class="table table-bordered table-striped">
    <tr><td><form:form action="/contacts/edit/${contact.id}.html" method="post" cssClass="form-horizontal registrationForm">

      <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name:</label>
        <div class="col-sm-10">
          <input type="text" id="name" name="name" class="form-control" value="${contact.name}"/>
        </div>
      </div>


      <div class="form-group">
        <div class="col-sm-2">
          <input type="submit" value="Save" class="btn btn-lg btn-primary" />
        </div>
      </div>
    </form:form></td></tr>

  </table>
</div>
</body>
</html>
