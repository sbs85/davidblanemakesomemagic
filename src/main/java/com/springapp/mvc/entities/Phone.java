package com.springapp.mvc.entities;

import javax.persistence.*;

@Entity
public class Phone extends BaseEntity {

    @Column
    private String number;

    @ManyToOne
    private Contact contact;

    @ManyToOne
    private PhoneType phoneType;

    public Phone(){
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public PhoneType getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }
}
