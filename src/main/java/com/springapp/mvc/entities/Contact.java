package com.springapp.mvc.entities;

import javax.persistence.*;
import javax.persistence.criteria.Order;
import java.util.List;
import java.util.Set;

@Entity
public class Contact extends BaseEntity {

    @Column
    private String name;

    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Phone> phones;

    public Contact(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }
}
