package com.springapp.mvc.repositories;

import com.springapp.mvc.entities.PhoneType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneTypeRepository extends JpaRepository<PhoneType,Long> {
}
