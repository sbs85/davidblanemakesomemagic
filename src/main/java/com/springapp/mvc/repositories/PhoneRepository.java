package com.springapp.mvc.repositories;

import com.springapp.mvc.entities.Contact;
import com.springapp.mvc.entities.Phone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PhoneRepository extends JpaRepository<Phone,Long> {
    List<Phone> findByContact(Contact contact);

    @Query("select p from Phone p where upper(p.contact.name) like upper(:nameLike) or p.number like :numberLike")
    Page<Phone> findByNameOrPhoneLikeIgnoreCaseOrderByContactName(@Param("nameLike")String nameLike, @Param("numberLike") String numberLike, Pageable p);
}
