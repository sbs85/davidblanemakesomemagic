package com.springapp.mvc.controllers;

import com.springapp.mvc.entities.Contact;
import com.springapp.mvc.entities.Phone;
import com.springapp.mvc.entities.PhoneType;
import com.springapp.mvc.repositories.ContactRepository;
import com.springapp.mvc.repositories.PhoneRepository;
import com.springapp.mvc.repositories.PhoneTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ContactController {
    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private PhoneTypeRepository phoneTypeRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public String contactsList(Model model) {
        PageRequest pageRequest = new PageRequest(0, 5, Sort.Direction.ASC, "name");
        Page<Contact> contactsPage = contactRepository.findAll(pageRequest);
        int current = contactsPage.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, contactsPage.getTotalPages());

        model.addAttribute("totalPages", contactsPage.getTotalPages());
        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);
        model.addAttribute("contacts", contactsPage.getContent());
        return "contact/list";
    }

    @RequestMapping(value = "/contacts/page/{page}", method = RequestMethod.GET)
    public String contactsListPage(Model model, @PathVariable int page) {
        PageRequest pageRequest = new PageRequest(page-1, 5, Sort.Direction.ASC, "name");
        Page<Contact> contactsPage = contactRepository.findAll(pageRequest);
        int current = contactsPage.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, contactsPage.getTotalPages());

        model.addAttribute("totalPages", contactsPage.getTotalPages());
        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);
        model.addAttribute("contacts", contactsPage.getContent());
        return "contact/list";
    }

    @RequestMapping(value = "/contacts/{id}")
    public String contactDetails(Model model, @PathVariable Long id) {
        Contact contact = contactRepository.findOne(id);
        List<Phone> phones = phoneRepository.findByContact(contact);
        contact.setPhones(phones);
        model.addAttribute("contact", contact);
        return "contact/detail";
    }

    @RequestMapping(value = "/contacts/add", method = RequestMethod.GET)
    public String getAddContact(Model model) {
        model.addAttribute("phonetypes", phoneTypeRepository.findAll());
        return "contact/add";
    }

    @RequestMapping(value = "/contacts/add", method = RequestMethod.POST)
    public String postAddContact(@RequestParam String name,
                                 @RequestParam String phonenumber, @RequestParam Long phonetype_id) {
//        System.out.println("name="+name+", phonenumber="+phonenumber+", phonetype_id="+phonetype_id.toString());

        Contact contact = new Contact();
        contact.setName(name);
        contact = contactRepository.save(contact);

        PhoneType phoneType = phoneTypeRepository.findOne(phonetype_id);

        Phone phone = new Phone();
        phone.setNumber(phonenumber);
        phone.setPhoneType(phoneType);
        phone.setContact(contact);
        phone = phoneRepository.save(phone);

        return "redirect:/contacts/" + contact.getId().toString() + ".html";
    }

    @RequestMapping(value = "/contacts/edit/{id}", method = RequestMethod.GET)
    public String getEditContact(Model model, @PathVariable Long id) {
        model.addAttribute("contact", contactRepository.findOne(id));
        return "contact/edit";
    }

    @RequestMapping(value = "contacts/edit/{id}", method = RequestMethod.POST)
    String postEditContact(Model model, @PathVariable Long id, @RequestParam String name) {
        Contact contact = contactRepository.findOne(id);
        contact.setName(name);
        contact = contactRepository.save(contact);
        return "redirect:/contacts/" + contact.getId().toString() + ".html";
    }

    @RequestMapping(value = "/contacts/deletecontact/{id}")
    public String deleteContact(Model model, @PathVariable Long id) {
        contactRepository.delete(id);
        return "redirect:/contacts.html";
    }

    @RequestMapping(value = "contacts/deletephone/{id}")
    String deletePhone(Model model, @PathVariable Long id) {
        Contact contact = phoneRepository.findOne(id).getContact();
        phoneRepository.delete(id);
        return "redirect:/contacts/" + contact.getId().toString() + ".html";
    }


    @RequestMapping(value = "/contacts/find")
    public String findContacts(Model model, @RequestParam String searchCriteria) {
        PageRequest pageRequest = new PageRequest(0, 5, Sort.Direction.ASC,"contact.name");
        Page<Phone> phonesPage = phoneRepository.findByNameOrPhoneLikeIgnoreCaseOrderByContactName("%" + searchCriteria + "%", "%" + searchCriteria + "%", pageRequest);
        int current = phonesPage.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, phonesPage.getTotalPages());

        model.addAttribute("totalPages", phonesPage.getTotalPages());
        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);

        List<Phone> foundPhones = phonesPage.getContent();
        model.addAttribute("foundPhones", foundPhones);
        model.addAttribute("searchCriteria", searchCriteria);

        return "contact/list";
    }

    @RequestMapping(value = "/contacts/find/page/{page}")
    public String findContactsPage(Model model, @PathVariable int page, @RequestParam String searchCriteria) {
        PageRequest pageRequest = new PageRequest(page-1, 5, Sort.Direction.ASC,"contact.name");
        Page<Phone> phonesPage = phoneRepository.findByNameOrPhoneLikeIgnoreCaseOrderByContactName("%" + searchCriteria + "%", "%" + searchCriteria + "%", pageRequest);
        int current = phonesPage.getNumber() + 1;
        int begin = Math.max(1, current - 5);
        int end = Math.min(begin + 10, phonesPage.getTotalPages());

        model.addAttribute("totalPages", phonesPage.getTotalPages());
        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);

        List<Phone> foundPhones = phonesPage.getContent();
        model.addAttribute("foundPhones", foundPhones);
        model.addAttribute("searchCriteria", searchCriteria);

        return "contact/list";
    }

}
