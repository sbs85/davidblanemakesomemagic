package com.springapp.mvc.controllers;

import com.springapp.mvc.entities.Contact;
import com.springapp.mvc.entities.Phone;
import com.springapp.mvc.entities.PhoneType;
import com.springapp.mvc.repositories.ContactRepository;
import com.springapp.mvc.repositories.PhoneRepository;
import com.springapp.mvc.repositories.PhoneTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PhoneController {
    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private PhoneTypeRepository phoneTypeRepository;

    @RequestMapping(value = "/phones/add", method = RequestMethod.GET)
    String getAddPhone(Model model, @RequestParam Long contact_id){
        Contact contact = contactRepository.findOne(contact_id);
        model.addAttribute("contact", contact);
        model.addAttribute("phonetypes",phoneTypeRepository.findAll());
        return "phone/add";
    }

    @RequestMapping(value = "/phones/add", method = RequestMethod.POST)
    String postAddPhone(Model model, @RequestParam Long contact_id, @RequestParam String phonenumber, @RequestParam Long  phonetype_id){
        Contact contact = contactRepository.findOne(contact_id);
        PhoneType phoneType = phoneTypeRepository.findOne(phonetype_id);
        Phone phone = new Phone();
        phone.setNumber(phonenumber);
        phone.setContact(contact);
        phone.setPhoneType(phoneType);
        phone = phoneRepository.save(phone);
        return "redirect:/contacts/"+contact_id.toString()+".html";
    }


    @RequestMapping(value = "/phones/edit/{id}", method = RequestMethod.GET)
    String getEditPhone(Model model, @PathVariable Long id){
        Phone phone = phoneRepository.findOne(id);
        Contact contact = phone.getContact();
        model.addAttribute("phone", phone);
        model.addAttribute("contact", contact);
        model.addAttribute("phonetypes",phoneTypeRepository.findAll());
        return "phone/edit";
    }

    @RequestMapping(value = "/phones/edit/{id}", method = RequestMethod.POST)
    String postEditPhone(Model model, @PathVariable Long id, @RequestParam String phonenumber, @RequestParam Long  phonetype_id){
        PhoneType phoneType = phoneTypeRepository.findOne(phonetype_id);
        Phone phone = phoneRepository.findOne(id);
        phone.setNumber(phonenumber);
        phone.setPhoneType(phoneType);
        phone = phoneRepository.save(phone);
        return "redirect:/contacts/"+phone.getContact().getId().toString()+".html";
    }
}
