package com.springapp.mvc.service;

import com.springapp.mvc.entities.Contact;
import com.springapp.mvc.entities.Phone;
import com.springapp.mvc.entities.PhoneType;
import com.springapp.mvc.repositories.ContactRepository;
import com.springapp.mvc.repositories.PhoneRepository;
import com.springapp.mvc.repositories.PhoneTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Set;

@Transactional
@Service
public class InitDbService {
    @Autowired
    private PhoneTypeRepository phoneTypeRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private ContactRepository contactRepository;

    @PostConstruct
    public void init(){
        if (phoneTypeRepository.count() == 0) {
            PhoneType typeMobie = new PhoneType();
            typeMobie.setPhoneType("Mobile");
            phoneTypeRepository.save(typeMobie);

            PhoneType typeHome = new PhoneType();
            typeHome.setPhoneType("Home");
            phoneTypeRepository.save(typeHome);

            PhoneType typeWork = new PhoneType();
            typeWork.setPhoneType("Work");
            phoneTypeRepository.save(typeWork);

            for(Integer i=0; i<7; i++) {
                Contact johnDoeContact = new Contact();
                johnDoeContact.setName("John Doe"+i.toString());
                contactRepository.save(johnDoeContact);

                Phone mobilePhone = new Phone();
                mobilePhone.setNumber("+770733112XX");
                mobilePhone.setContact(johnDoeContact);
                mobilePhone.setPhoneType(typeMobie);
                phoneRepository.save(mobilePhone);

                Phone workPhone = new Phone();
                workPhone.setNumber("+777725253XX");
                workPhone.setContact(johnDoeContact);
                workPhone.setPhoneType(typeWork);
                phoneRepository.save(workPhone);
            }
        }
    }
}
